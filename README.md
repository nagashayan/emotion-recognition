This repository is for implementing face recognition with emotion detection

It is CSC 8980 COMPUTER VISION class final project.

Data set used - Cohn Kanade Extended from CMU.

The project has two parts

1. Face detection and recognition
2. Emotion recognition

Prerequisites 

1. Install Python 2.7.* from here https://www.python.org/downloads/
2. Install pip -> python package manager from here https://pip.pypa.io/en/stable/installing/
3. Install cv2, Image, numpy, shutil, glob using pip
Ex: pip install cv2 
    pip install Image 


**Face detection and recognition
**
To run this module

1. Login to your facebook before running this, This program will select any 3 of your friends and fetch all their photos where their face was tagged by facebook already.
2. It will clean dataset by cropping face and converting into gray. It will convert the image into 100*100 pixels.
3. It will train the model with availabe data set and test using same data by using classification technique.
4. Haar classifier is used for identifying face and SVM is used for classification. 

Run the following commands:
python create_pics_from_facebook.py 
python match_faces.py


**Emotion recognition
**

1. You need to have CK+ dataset inside your project, which you can request from here http://www.consortium.ri.cmu.edu/ckagree/
2. Create 8 folders of different emotions inside your project under sorted_image and dataset
3. Create folder named difficult and source_images
4. Run Extract, then followed by extractimages and finally process_model

This will run for a while and do 10 iterations and give you accuracy.
It will sort images then crop it followed by converting to gray scale images.
Then it will use fisher algorithm to find the emotions.

Run the following commands:
python extract.py
python extractimages.py
python process_model.py